import {Book} from './book';
import {Album} from './album';

export const BOOKS: Book[] = [
  { title: '1984', authors: [ 'George Orwell' ]},
  { title: 'Animal Farm', authors: [ 'George Orwell' ]},
  { title: 'George Orwell biography', authors: [ 'George Orwell', 'Some other guy' ]},
  { title: 'Sapiens', authors: [ 'Yuval Noah Harari' ]},
  { title: 'Sapiens', authors: [ 'Yuval Noah Harari' ]},
  { title: 'Home Deus', authors: [ 'Yuval Noah Harari' ]},
  { title: '21 Lessons for the 21st Century', authors: [ 'Yuval Noah Harari' ]},
  { title: 'A Brief History of Time', authors: [ 'Stephen Hawking' ]},
];

export const ALBUMS: Album[] = [
  { title: 'Nevermind', artist:  'Nirvana' },
  { title: 'Bohemian Rapsody', artist:  'Queen' },
  { title: 'Ten', artist:  'Pearl Jam' },
  { title: 'Jagged Little Pill', artist:  'Alanis Morissette' },
  { title: 'Unplugged', artist:  'Alanis Morissette' },
  { title: 'Metallica', artist:  'Metallica' },
  { title: '...And Justice for All', artist:  'Metallica' },
  { title: 'Master of Puppets', artist:  'Metallica' },
];
