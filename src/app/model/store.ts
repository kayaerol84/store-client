import {Album} from './album';
import {Book} from './book';

export class Store {
  books: Book[];
  albums: Album[];
}
