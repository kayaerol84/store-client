import {Injectable} from '@angular/core';
import {Book} from '../model/book';
import {BOOKS, ALBUMS} from '../model/mock-books-albums';
import {Album} from '../model/album';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {Store} from '../model/store';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private storeUrl = 'http://localhost:8080/api/store';  // URL to web api
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };


  constructor(private http: HttpClient) {
  }

  // Deprecated
  getBooks(): Observable<Book[]> {
    return of(BOOKS);
  }

  getAlbums(): Observable<Album[]> {
    return of(ALBUMS);
  }

  /* GET books & albums whose name contains search term */
  searchStore(term: string): Observable<Store> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of();
    }
    return this.http.get<Store>(`${this.storeUrl}?input=${term}`)
      .pipe(
        tap(_ => console.log(`found books and albums matching "${term}"`)),
        catchError(this.handleError<Store>('searchStore', ))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

