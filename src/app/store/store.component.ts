import { Component, OnInit } from '@angular/core';
import { Book } from '../model/book';
import { Album } from '../model/album';
import { ALBUMS, BOOKS } from '../model/mock-books-albums';
import { StoreService } from '../service/store.service';
import {Observable, Subject} from 'rxjs';
import {Store} from '../model/store';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {DeprecatedCommand} from '@angular/cli/commands/deprecated-impl';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  books: Book[];
  albums: Album[];
  selectedBook: Book;
  selectedAlbum: Album;

  store$: Observable<Store>;
  private searchTerms = new Subject<string>();

  constructor(private storeService: StoreService) {
  }
  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
    this.store$ = this.storeService.searchStore(term);
    this.store$.subscribe(store => {
      this.books = store.books;
      this.albums = store.albums;
    });
  }

  ngOnInit() {
  }


  onBookSelect(book: Book): void {
    this.selectedBook = book;
  }

  onAlbumSelect(album: Album): void {
    this.selectedAlbum = album;
  }

  // Deprecated
  getStoreItems(): void {
    this.storeService.getBooks().subscribe(
      books => this.books = books
    );
    this.storeService.getAlbums().subscribe(
      albums => this.albums = albums
    );
  }
}
